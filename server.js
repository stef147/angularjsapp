//Install express server
const express = require('express');
const path = require('path');

const app = express();

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist'));

// app.get('/index.html', function(req,res) {
//   console.log('__dirname:'+__dirname);
//   res.sendFile(path.join(__dirname+'/dist/firstAng6App/index.html'));
// });
// app.get('/firstAng6App.js', function(req,res) {
//   console.log('__dirname:'+__dirname);
//   console.log(req);
//   res.sendFile(path.join(__dirname+'/dist/firstAng6App/firstAng6App.js'));
// });

console.log("Starting server on port 5207");
app.listen(process.env.PORT || 5207);
