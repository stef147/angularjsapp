
import '../style/app.css';


angular.module('app')
  .component('app', {
    template: require('./app.html'),
    controller: ['$scope', appController],
    controllerAs: 'appCtrl',
  });

function appController($scope) {
  var appCtrl = this;
  appCtrl.name = 'Stefan';

  var scripts = ['http://localhost:5201/firstAng6App.js',
    'http://localhost:5202/secondAng6App.js',
    'https://unpkg.com/@stef147/stencilapp/dist/stencil-component.js'
  ];

  // var scripts = ['http://10.0.2.2:5201/firstAng6App.js',
  //   'http://10.0.2.2:5202/secondAng6App.js',
  //   // 'https://unpkg.com/@stef147/stencilapp/dist/stencil-component.js'
  // ];


  loadScripts();

  addEventListeners();

  appCtrl.update = function() {
      console.log('update');
    appCtrl.name = 'Stefan';
    };

  appCtrl.change = function() {
      console.log('update');
    appCtrl.name = 'Bob';
    };

  appCtrl.messageListener = function($event) {
      console.log('messageListener');
    };

  function loadScripts() {
    angular.forEach(scripts, function(script) {
      var element = document.createElement("script");
      element.type = "text/javascript";
      element.src = script;
      document.body.appendChild(element);
    })
  }

  function addEventListeners() {
    const firstApp = document.querySelector('first-ang-app');
    const secondApp = document.querySelector('second-ang-app');
    firstApp.addEventListener('message', (data) => {
      console.log(data.detail);
      appCtrl.name = data.detail;
      $scope.$apply();
    });

    secondApp.addEventListener('message', (data) => {
      console.log(data.detail);
      appCtrl.name = data.detail;
      $scope.$apply();
    });
  }
}

