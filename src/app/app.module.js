import angular from 'angular';

let moduleName = 'app';

angular.module(moduleName, []);

require('./app');

module.exports = moduleName;
